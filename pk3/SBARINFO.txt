monospacefonts true, "0";
//interpolatehealth true, 2;
//interpolateArmor true, 1;
completeborder true;

statusbar Fullscreen, fullscreenoffsets
{


	InInventory NOT HudCleaner //Don't draw anything if the player is dead.
	{
		InInventory HudAlive
		{                 
			
			InInventory MurderMarker
			{
				DrawBar "METRFRNT", "HUDBAK1", MadMeter, horizontal, interpolate(2), 8, -54;
			}
			DrawImage translatable "PLAYMARK", 325, -41;
			DrawBar "HEALF1", "HEALB1", health, horizontal, interpolate(2), 28, -43;
			DrawBar "STAMF1", "STAMB1", PlayerStamina, horizontal, interpolate(2), 30, -25;
			
			InInventory MurderMarker
			{
				/*DrawBar "SANEBCK1", "SANEFRT1", MadMeter2, horizontal, interpolate(2), reverse, 28, -43
				UGLY UGLY UGLY */
				DrawImage "HUDBORD2", 28, -47; 
			}

			InInventory NOT MurderMarker
			{
				DrawImage "HUDBORD1", 28, -43; 
			}
		

			//Medikit/Stamkit drawing block thing
			//===========================================
			InInventory SmallKit, 1
			{
				DrawImage "STIMA0", 44, -45;

			}

			InInventory SmallKit, 2
			{
				DrawImage "STIMA0", 60, -45;

			}

			InInventory SmallKit, 3
			{
				DrawImage "STIMA0", 76, -45;

			}

			InInventory BigKit, 1
			{
				DrawImage "MEDIA0", 59, -62;
			}
			
			InInventory StamKit, 1
			{
				DrawImage "STMKA0", 102, -45;
			}
			
			InInventory StamKit, 2
			{
				DrawImage "STMKA0", 134, -45;
			}

			//===========================================
			//Inventory
			
			DrawImage "SINVBACK", 365, -63;
			drawselectedinventory SMALLFONT, 365, -63;
			
			//==========================================
			// Antagonist resist pip. 
			InInventory PowerResist00 { DrawImage "RESISTMK", 84, -47; }
			InInventory PowerResist15 { DrawImage "RESISTMK", 144, -47; }
			InInventory PowerResist30 { DrawImage "RESISTMK", 204, -47; }
			InInventory PowerResist45 { DrawImage "RESISTMK", 264, -47; }
		
			/*
			IsSelected Shotgun2
			{
					//drawimage "D3S1C0", 414, 163, centerbottom;
					drawimage ammoicon1, 414, 179, centerbottom;
			}

			IsSelected Beretta
			{
					//drawimage "CLP4A0", 414, 163, centerbottom;
					drawimage ammoicon1, 414, 179, centerbottom;
			}

			IsSelected M1Carbine
			{
					//drawimage "M1CLIP", 414, 163, centerbottom;
					drawimage ammoicon1, 414, 179, centerbottom;
			}
			*/
			
			DrawImage "SINVBACK", 453, -63;
			
			InInventory PipeWrench { DrawImage "PIPPBH", 488, -20, centerbottom; }
			InInventory LeadPipe { DrawImage "PIPPAH", 488, -20, centerbottom; }
			InInventory PoolCue { DrawImage "POLCUE", 488, -20, centerbottom; }
			InInventory Shovel { DrawImage "SHOVAH", 488, -20, centerbottom; }
			InInventory Molotov { DrawImage "MOLPAH", 488, -20, centerbottom; }
			InInventory Brick { DrawImage "BRKTAH", 488, -20, centerbottom; }
			InInventory LChainsaw
			{
				DrawImage "CSAWAH", 488, -20, centerbottom;
			}
			InInventory Crossbow
			{
				DrawImage "XBOWICO", 488, -20, centerbottom;
			}
			InInventory Shotgun2
			{
				DrawImage "DHSPAH", 488, -20, centerbottom;
				DrawNumber 4, SMALLFONT, red, Ammo SuperShells, 502, -26;
			}
			InInventory Beretta
			{
				DrawImage "PISTAH", 488, -20, centerbottom;
				DrawNumber 4, SMALLFONT, red, Ammo PistolAmmo, 502, -26;
			}
			InInventory M1Carbine
			{
				DrawImage "M1GNYH", 488, -20, centerbottom;
				DrawNumber 4, SMALLFONT, red, Ammo M1CarbineClipIn, 502, -26;
			}
			
			IsSelected LChainsaw 
			{
				DrawBar "GASFRONT", "GASBACK", Gasoline, Vertical, 532, -52;
			}
			
		}
	}
}


/*
statusbar inventoryfullscreen, fullscreenoffsets
{
	DrawInventoryBar Strife, alwaysshowcounter, noarrows, translucent, vertical, 6, SMALLFONT, 365, -367, -1, 120;
}
*/

statusbar inventoryfullscreen
{
	//drawinventorybar Doom, 7, INDEXFON, 50, 100;
	DrawInventoryBar Strife, alwaysshowcounter, noarrows, translucent, 6, SMALLFONT, -42, 50, -1, 120;
	//DrawString SMALLFONT, Gold, inventorytag, , <y> [, <spacing> [, <flags>]] //For when Skulltag goes to 2.5.0 :(
}

statusbar normal
{

InInventory NOT HudCleaner
{
InInventory HudAlive
	{
	InInventory NOT MurderMarker, 1 //This seemed like a nice backdrop, imo.
	{
		DrawImage "EMPLEFT", -249, 128;
		DrawImage "EMPRIGHT", 163, 128;
    	}


	InInventory MurderMarker, 1 //If the player is a murderer, draw the insanity bar. Wheeeeeee
	{
		DrawBar "FULLEFT", "EMPLEFT", MadMeter, horizontal, Reverse, -249, 128; //Left side
		DrawBar "FULRIGHT", "EMPRIGHT", MadMeter, horizontal, 163, 128; //Right side
	}


	//Medikit drawing block thing
	//===========================================
	InInventory SmallKit, 1
	{
		DrawImage "STIMA0", -144, 122;

	}

	InInventory SmallKit, 2
	{
		DrawImage "STIMA0", -128, 122;

	}

	InInventory SmallKit, 3
	{
		DrawImage "STIMA0", -112, 122;

	}

	InInventory BigKit, 1
	{
		DrawImage "MEDIA0", -129, 104;
	}
	
	InInventory StamKit, 1
	{
		DrawImage "STMKA0", 147, 122;
	}
			
	InInventory StamKit, 2
	{
		DrawImage "STMKA0", 179, 122;
	}

	//===========================================

	AspectRatio "4:3"
	{

	}
	
	AspectRatio "16:9"
	{
	}

//The Health background with translatable color. Wheeeeeeeeee
DrawImage translatable "HUDCOLOR", -154, 140;
//Numbers
//drawnumber 1, SMALLFONT, red, SmallKit, -102, 144; 
drawnumber 3, HUDFONT_DOOM, red, health, -112, 148; 
//DrawString HUDFONT_DOOM, yellow, "/", -104, 156;
drawnumber 3, HUDFONT_DOOM, green, PlayerStamina, -92, 162; 
//drawnumber 3, HUDFONT_DOOM, yellow, Aggro, -92, 100; //Aggro for testing plzignore

//Mugshot hack///////////////////////////////////////////////////
//DrawMugShot 1, disablegrin, disableouch, disablerampage, 0, 0; 
/////////////////////////////////////////////////////////////////

	//IsSelected FistWithLighter 
	//{
	//	DrawBar "LITFRONT", "LITBACK", Lighterammo, Vertical, 432, 142;
	//}

	IsSelected Shotgun2
	{
    		drawimage "D3S1C0", 414, 163, centerbottom;
    		drawimage ammoicon1, 414, 179, centerbottom;
	}

	IsSelected Beretta
	{
    		drawimage "CLP4A0", 414, 163, centerbottom;
    		drawimage ammoicon1, 414, 179, centerbottom;
	}

	IsSelected M1Carbine
	{
    		drawimage "M1CLIP", 414, 163, centerbottom;
    		drawimage ammoicon1, 414, 179, centerbottom;
	}

	//IsSelected not Beretta, Shotgun2
	//{
    	//	drawimage ammoicon2, 414, 163, centerbottom;
	//}

	IsSelected NOT LChainsaw 
	{
    		drawimage ammoicon1, 414, 179, centerbottom;
		drawnumber 4, HUDFONT_DOOM, red, ammo2, 464, 148;
		drawnumber 4, HUDFONT_DOOM, red, ammo1, 464, 164;
	}

	IsSelected LChainsaw 
	{
		DrawBar "GASFRONT", "GASBACK", Gasoline, Vertical, 432, 142;
	}

//drawnumber 4, BIGFONT, yellow, PowerupTime MeleeSlower, whennotzero, 166, 64;

	InInventory SlowCounter
	{
    		drawimage "STUNICO", 166, 102, centerbottom;
		drawbar "STUNBA", "STUNBAK", SlowCounter, horizontal, 140, 112;
	}

//============================================


DrawImage "SINVBACK", 462, 100;
drawselectedinventory SMALLFONT, 462, 100, 528, 116;
}
}
}

statusbar inventory
{
	//drawinventorybar Doom, 7, INDEXFON, 50, 100;
	DrawInventoryBar Strife, alwaysshowcounter, noarrows, translucent, 6, SMALLFONT, -42, 50, -1, 120;
	//DrawString SMALLFONT, Gold, inventorytag, , <y> [, <spacing> [, <flags>]] //For when Skulltag goes to 2.5.0 :(
}